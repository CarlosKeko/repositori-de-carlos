import java.util.Scanner;

/**
 * El cl�ssic joc del buscamines, amb intef�cie.
 * @version 3.0
 * Aixó és una prova
 * @author Carlos Urbina
 * 
 */
public class BuscaminesAmbInterficie {

	public static Taulell t = new Taulell();
	public static Finestra f = new Finestra(t);
	public static Scanner reader = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String partida;

		do {
			System.out.println("Escriu el nom del jugador");
			partida = joc();

		} while (partida.equals("SI"));
	}

	static void pulsarEnter() { // Funció per polsar enter, serveix com separador de linia entre execuci� i execuci� de processos.
		String enter = "";
		System.out.print("\nPrem enter per continuar" + enter);
		reader.nextLine();
		enter = reader.nextLine();

	}

	static int escollirDificultat() { // Funció que serveix per escollir la dificultat al principi del joc, depenen de la dificultat apareixeran m�s o menys caselles i mines.
		int escollir;

		System.out.println("\nEscull la dificultat");
		System.out.println("\n[1] Principiant");
		System.out.println("[2] Intermedi");
		System.out.println("[3] Expert");
		System.out.println("[4] Personalitzat");
		System.out.println("[0] Informació dificultat");

		System.out.print("\nTria una opció: ");
		escollir = reader.nextInt();

		return escollir;
	}

	static void mostrarInformacio() { // Funció per mostrar la informaci� de les dificultats disponibles.
		System.out.println("\nNivell principiant: 8 � 8 caselles i 10 mines.");
		System.out.println("Nivell intermedi: 16 x 16 caselles i 40 mines.");
		System.out.println("Nivell expert: 16 x 30 caselles i 99 mines.");
		System.out.println("Nivell personalitzat: l'usuari escull les caselles i les mines.");

	}

	static int menuJugar() { // Funci� que serveix per presentar el men� i tamb� per escollir l'opci�.
		int escollir;

		System.out.println("\n[1]. Mostrar casella");
		System.out.println("[2]. Marcar possible mina");
		System.out.println("[3]. Desmarcar possible mina");
		System.out.println("[4]. Estad�stiques actuals i veure tauler");
		System.out.println("[0]. Sortir");

		System.out.print("\nTria una opci�: ");
		escollir = reader.nextInt();

		return escollir;
	}

	static String[][] inicialitzarTauler(int files, int columnes, boolean dificultatPersonalitzada, int mines) { // Funci�  per inicialitzar la matriu del tauler que trucar� a altres funcions.
		String[][] matriu = escollirFilesColumnes(files, columnes, dificultatPersonalitzada, mines);
		matriu = iniciarCaselles(matriu);
		matriu = posarMines(matriu, mines, dificultatPersonalitzada);
		matriu = destaparCasellesBackEnd(matriu);

		return matriu;

	}

	static String[][] escollirFilesColumnes(int numFiles, int numColumnes, boolean dificultatPersonalitzada, int mines) { // Funci� que estableix la mida de la matriu modificant les variables locals de files i columnes, tamb� estableix la mida a la matriu.
		if (dificultatPersonalitzada) { // Depenen de la dificultat que hem escollit entrar� per aquest if.
			while (numFiles < 1) {
				System.out.print("Escriu el nombre de files que vols per al teu tauler, major a 0: ");
				numFiles = reader.nextInt();

				if (numFiles < 1) {
					System.out.println("Valor de files incorrecte, torna a provar");
				}

			}

			while (numColumnes < 1) {
				System.out.print("Escriu el nombre de columnes que vols per al teu tauler, major a 0: ");
				numColumnes = reader.nextInt();

				if (numColumnes < 1) {
					System.out.println("Valor de columnes incorrecte, torna a provar");
				}
			}

		}

		String[][] matriuAuxiliar = new String[numFiles][numColumnes];

		return matriuAuxiliar;
	}

	static String[][] iniciarCaselles(String[][] matriu) { // Funci� per posar totes les caselles de la matriu com no descobertes.

		for (int i = 0; i < matriu.length; i++) {
			for (int j = 0; j < matriu[i].length; j++) {
				matriu[i][j] = "[ ]";
			}
		}

		return matriu;
	}

	static String[][] posarMines(String[][] matriu, int mines, boolean dificultatPersonalitzada) { // Funci� per posar les mines del tauler

		int filaAux, colAux;

		if (dificultatPersonalitzada) { // Aquest if serveix per quan l'usuari escull dificultat personalitzada, demanar-li les mines del tauler
			while (mines < 1 || mines > ((matriu.length * matriu[0].length) - 1)) {
				System.out.print(
						"Escull el numero de mines entre 1 i " + ((matriu.length * matriu[0].length) - 1) + ": ");
				mines = reader.nextInt();

				if (mines < 1 || mines > ((matriu.length * matriu[0].length) - 1)) {
					System.out.println("Quantitat de mines no valida, torna a provar");
				}
			}

		}

		while (mines > 0) {
			filaAux = (int) (Math.random() * matriu.length);
			colAux = (int) (Math.random() * matriu[0].length);

			if (matriu[filaAux][colAux].equals("[ ]")) {
				matriu[filaAux][colAux] = "[*]";
				mines--;

			}
		}

		return matriu;
	}

	static String[][] destaparCasellesBackEnd(String[][] matriu) { // Funci� que serveix per posar al tauler secret a totes caselles quantes mines hi ha al seu voltant.

		int mines = 0;
		String aux = "[ ]";

		for (int i = 0; i < matriu.length; i++) {
			for (int j = 0; j < matriu[0].length; j++) {
				if (!matriu[i][j].equals("[*]")) {
					mines = 0;
					aux = "[ ]";

					if (j + 1 < matriu[i].length && matriu[i][j + 1].equals("[*]")) {
						mines++;

					}
					if (j - 1 >= 0 && matriu[i][j - 1].equals("[*]")) {
						mines++;

					}
					if (i - 1 >= 0 && matriu[i - 1][j].equals("[*]")) {
						mines++;

					}
					if (i + 1 < matriu.length && matriu[i + 1][j].equals("[*]")) {
						mines++;

					}
					if (i - 1 >= 0 && j + 1 < matriu[i].length && matriu[i - 1][j + 1].equals("[*]")) {
						mines++;

					}
					if (i + 1 < matriu.length && j + 1 < matriu[i].length && matriu[i + 1][j + 1].equals("[*]")) {
						mines++;

					}
					if (i - 1 >= 0 && j - 1 >= 0 && matriu[i - 1][j - 1].equals("[*]")) {
						mines++;

					}
					if (i + 1 < matriu.length && j - 1 >= 0 && matriu[i + 1][j - 1].equals("[*]")) {
						mines++;

					}

					aux = aux.replace(" ", Integer.toString(mines));
					matriu[i][j] = aux;

				}
			}
		}

		return matriu;
	}

	static void mostraTauler(String[][] tauler) { // Funció que es fa servir per mostrar el tauler, hem de posar, que pugui rebre la matriu tauler.

		System.out.println("\nTAULER");

		for (int j = 0; j < tauler[0].length; j++) {
			System.out.print("\t " + j);
		}
		System.out.println("\n");

		for (int i = 0; i < tauler.length; i++) {
			System.out.print(i + "\t");

			for (int j = 0; j < tauler[i].length; j++) {
				System.out.print(tauler[i][j] + "\t");

			}
			System.out.println("\n");
		}

		System.out.println("[ ] = Casella no descoberta \t\t [*] = Mina");
		System.out.println("[?] = Possible mina marcadada \t\t [0 - 8] = Mines al voltant de la casella");

	}

	static String[][] mostraCasella(String[][] matriuFrontEnd, String[][] matriuBackEnd) { // Funció per mostrar una casella, aquesta funci� trucar� a unes altres
		int fila = -1, columna = -1;
		
		t.mousecol = -1;
		t.mousefil = -1;
		

		while (fila == -1 && columna == -1) {
			fila = t.getMousefil();
			columna = t.getMousecol();
			System.out.println();
		}

		if (fila != -1 && columna != -1) {
			matriuFrontEnd[fila][columna] = matriuBackEnd[fila][columna];
			matriuFrontEnd = recursivitatCaselles(matriuFrontEnd, matriuBackEnd, fila, columna);

			for (int i = 0; i < matriuFrontEnd.length; i++) {
				for (int j = 0; j < matriuFrontEnd.length; j++) {
					if (matriuFrontEnd[i][j].equals("revisat")) {
						matriuFrontEnd[i][j] = matriuFrontEnd[i][j].replace("revisat", "[0]");
					}
				}
			}
		}

		return matriuFrontEnd;
	}

	static int[] escollirCasella(String[][] matriuBackEnd) { // Funció per escollir una casella demanant a l'usuari la fila i columna
		int fila = -1, columna = -1;
		
		while (columna == -1 && fila == -1) {
			columna = t.getMousecol();
			fila = t.getMousefil();

		}

		int[] posicions = { fila, columna };
		return posicions;

	}

	static String[][] comprovacioCasella(String[][] matriuFrontEnd, String[][] matriuBackEnd) { // Funci� que comprova l'estat de la casella escollida i truca a un altra funci� per expandir les possibles caselles.
		int[] posicions = new int[2];

		while (posicions[0] == -1 && posicions[1] == -1) {
			posicions[0] = t.getMousecol();
			posicions[1] = t.getMousefil();

		}

		if (matriuBackEnd[posicions[0]][posicions[1]].equals("[*]")) {
			System.out.println("\nHas explotat una mina");
			matriuFrontEnd[posicions[0]][posicions[1]] = matriuBackEnd[posicions[0]][posicions[1]];

		} else {
			matriuFrontEnd[posicions[0]][posicions[1]] = matriuBackEnd[posicions[0]][posicions[1]];
			matriuFrontEnd = recursivitatCaselles(matriuFrontEnd, matriuBackEnd, posicions[0], posicions[1]);

			for (int i = 0; i < matriuFrontEnd.length; i++) {
				for (int j = 0; j < matriuFrontEnd.length; j++) {
					if (matriuFrontEnd[i][j].equals("revisat")) {
						matriuFrontEnd[i][j] = matriuFrontEnd[i][j].replace("revisat", "[0]");
					}
				}
			}

			System.out.println("\nLa casella s'ha desmarcat i no hi havia una mina!");

		}

		return matriuFrontEnd;
	}

	static String[][] recursivitatCaselles(String[][] matriuFrontEnd, String[][] matriuBackEnd, int i, int j) {

		int[] fila = { 0, 0, -1, 1, -1, -1, 1, 1 };
		int[] columna = { 1, -1, 0, 0, 1, -1, 1, -1 };

		if (i < matriuFrontEnd.length && j < matriuFrontEnd[0].length) {
			if (matriuFrontEnd[i][j].equals("[0]") && !matriuFrontEnd[i][j].equals("revisat")
					&& !matriuFrontEnd[i][j].equals("[?]")) {
				matriuFrontEnd[i][j] = "revisat";

				for (int posicio = 0; posicio < fila.length; posicio++) {
					if (i + fila[posicio] >= 0 && i + fila[posicio] < matriuFrontEnd.length && j + columna[posicio] >= 0 && j + columna[posicio] < matriuFrontEnd[0].length && !matriuFrontEnd[i + fila[posicio]][j + columna[posicio]].equals("revisat") && !matriuFrontEnd[i + fila[posicio]][j + columna[posicio]].equals("[?]")) {
						matriuFrontEnd[i + fila[posicio]][j + columna[posicio]] = matriuBackEnd[i + fila[posicio]][j+ columna[posicio]];
					}
				}
				i = 0;
				j = -1;

			}
			j++;
			if (j == matriuFrontEnd[0].length) {
				j = 0;
				i++;
			}

			recursivitatCaselles(matriuFrontEnd, matriuBackEnd, i, j);
		}

		return matriuFrontEnd;
	}

	static int comprovarEstatTauler(String[][] matriuFrontEnd, String[][] matriuBackEnd) { // Funci� per comprovar si el joc continua o no.
		int opcio = 1;
		boolean diferencia = false;
		String[][] aux = new String[matriuFrontEnd.length][matriuFrontEnd[0].length];

		for (int i = 0; i < matriuFrontEnd.length; i++) {
			for (int j = 0; j < matriuFrontEnd[0].length; j++) {
				if (!matriuBackEnd[i][j].equals("[*]")) {
					aux[i][j] = matriuBackEnd[i][j];

				} else {
					aux[i][j] = "[ ]";
				}
			}
		}

		for (int i = 0; i < matriuFrontEnd.length; i++) {
			for (int j = 0; j < matriuFrontEnd[0].length; j++) {
				if (matriuFrontEnd[i][j].equals("[*]")) {
					opcio = 0;

					for (int fila = 0; fila < matriuBackEnd.length; fila++) {
						for (int columna = 0; columna < matriuBackEnd[0].length; columna++) {
							if (matriuBackEnd[fila][columna].equals("[*]")) {
								matriuFrontEnd[fila][columna] = matriuBackEnd[fila][columna];
							}
						}
					}

					int[][] taulerPerLaGUI = crearMatriuPerLaGUI(matriuFrontEnd);
					t.dibuixa(taulerPerLaGUI);

				}

				if (!matriuFrontEnd[i][j].equals(aux[i][j])) {
					diferencia = true;

				}

			}
		}

		if (!diferencia) {
			int[][] taulerPerLaGUI = crearMatriuPerLaGUI(aux);
			t.dibuixa(taulerPerLaGUI);
			System.out.println("\nTotes les caselles sense mina s'han mostrat, has guanyat!");
			opcio = 0;

		} else {
			for (int i = 0; i < matriuFrontEnd.length; i++) {
				for (int j = 0; j < matriuFrontEnd[0].length; j++) {
					if (matriuBackEnd[i][j].equals("[ ]")) {
						matriuBackEnd[i][j] = "[*]";

					}
				}
			}
		}

		return opcio;
	}

	static String[][] marcarPossibleMina(String[][] matriuFrontEnd) { // Funci� que serveix per marcar una possible mina

		int[] posicions = escollirCasella(matriuFrontEnd);

		if (matriuFrontEnd[posicions[0]][posicions[1]].equals("[ ]")) {
			System.out.println("\nLa casella s'ha marcat com possible mina");
			matriuFrontEnd[posicions[0]][posicions[1]] = "[?]";

		} else if (matriuFrontEnd[posicions[0]][posicions[1]].equals("[?]")) {
			System.out.println("\nNo es pot marcar aquesta casella, perqu� ja est� marcada com possible mina");

		} else {
			System.out.println("\nNo �s possible marcar aquest� casella perque ja est� descoberta");

		}

		return matriuFrontEnd;
	}

	static String[][] desmarcarPossibleMina(String[][] matriuFrontEnd) { // Funci� que serveix per desmarcar una possible mina

		int[] posicions = escollirCasella(matriuFrontEnd);

		if (matriuFrontEnd[posicions[0]][posicions[1]].equals("[?]")) {
			System.out.println("\nLa casella ja no est� marcada com possible mina");
			matriuFrontEnd[posicions[0]][posicions[1]] = "[ ]";

		} else {
			System.out.println("\nAquesta casella no est� marcada com possible mina");

		}

		return matriuFrontEnd;
	}

	static void mostrarEstadistiques(String[][] matriuFrontEnd, int mines) { // Funci� per veures les estadistiques actuals del joc

		int casellesNoDescobertes = 0, casellesDescobertes = 0;

		for (int i = 0; i < matriuFrontEnd.length; i++) {
			for (int j = 0; j < matriuFrontEnd[0].length; j++) {
				if (matriuFrontEnd[i][j].equals("[?]")) {
					mines--;

				} else if (matriuFrontEnd[i][j].equals("[ ]")) {
					casellesNoDescobertes++;

				} else {
					casellesDescobertes++;

				}
			}
		}

		System.out.println("\nAl tauler hi ha " + mines + " possibles mines, " + casellesNoDescobertes + " caselles no descobertes, " + casellesDescobertes + " caselles descobertes");
	}

	static String joc() { // Funci� per executar el joc

		int opcio = 0, files = 0, columnes = 0, mines = 0;
		boolean dificultatPersonalitzada = false;
		String[][] taulerBackEnd = null;
		String[][] taulerFrontEnd = null;
		int[][] taulerPerLaGUI = null;
		String partida = "";

		do {
			try {

				opcio = escollirDificultat();

				switch (opcio) {
				case 1:
					files = 8;
					columnes = 8;
					mines = 10;
					taulerBackEnd = inicialitzarTauler(files, columnes, dificultatPersonalitzada, mines);
					break;

				case 2:
					files = 16;
					columnes = 16;
					mines = 40;
					taulerBackEnd = inicialitzarTauler(files, columnes, dificultatPersonalitzada, mines);
					break;

				case 3:
					files = 16;
					columnes = 30;
					mines = 99;
					taulerBackEnd = inicialitzarTauler(files, columnes, dificultatPersonalitzada, mines);
					break;

				case 4:
					mines = 0;
					dificultatPersonalitzada = true;
					taulerBackEnd = inicialitzarTauler(files, columnes, dificultatPersonalitzada, mines);
					for (int i = 0; i < taulerBackEnd.length; i++) {
						for (int j = 0; j < taulerBackEnd[0].length; j++) {
							if (taulerBackEnd[i][j].equals("[*]")) {
								mines++;
							}
						}
					}
					break;

				case 0:
					mostrarInformacio();
					pulsarEnter();
					break;

				default:
					System.out.println("Valor incorrecte, torna a provar.");
					pulsarEnter();
					break;
				}

			} catch (Exception e) {
				System.out.println("Car�cter incorrecte, torna a provar.");
				pulsarEnter();

			}

		} while (opcio < 1 || opcio > 4);

		taulerFrontEnd = new String[taulerBackEnd.length][taulerBackEnd[0].length];
		taulerFrontEnd = iniciarCaselles(taulerFrontEnd);
		inicialitzarGUI();

		do {
			taulerPerLaGUI = crearMatriuPerLaGUI(taulerFrontEnd);
			t.dibuixa(taulerPerLaGUI);
			taulerFrontEnd = mostraCasella(taulerFrontEnd, taulerBackEnd);
			opcio = comprovarEstatTauler(taulerFrontEnd, taulerBackEnd);

		} while (opcio != 0);
		
		System.out.println("El joc ha finalitzat");
		System.out.print("Vols fer una altra partida (SI/NO)? ");
		while (!partida.equals("SI") && !partida.equals("NO")) {
			try {
				partida = reader.nextLine().toUpperCase().trim();
				

			} catch (Exception e) {
				System.out.println("Valor invalid, torna a provar");
				reader.next();

			}

		}

		return partida;
	}

	private static void inicialitzarGUI() {
		t.setActcolors(false); // activar colors
		t.setFons(0xb1adad); // color del fons (si els colors estan desactivats). Els colors estan donats en hexa, i comencen per 0x (forma d�indicar de qu� es tracta d�un nombre en hexa)
		t.setActimatges(true); // activar imatges
		t.setActlletres(false); // activar text
//        String[] lletres = {"","1","2","3","4","5","6","7","8","*","MINA"};  //qu� s'ha d'escriure en cada casella en base al nombre
		String[] imatges = { "t0.png", "t1.png", "t2.png", "t3.png", "t4.png", "t5.png", "t6.png", "t7.png", "t8.png", "download.png", "t-1.PNG" };
//        t.setLletres(lletres);  //passar la llista feta al taulell
		t.setImatges(imatges);
//        int[] colorlletres = {0x0000FF,0x00FF00,0xFFFF00,0xFF0000,0xFF00FF,0x00FFFF,0x521b98,0xFFFFFF,0xFF8000,0x7F00FF,0xFFFFFF};   //una llista de colors de lletres. La primera posici� ser� el color que correspongui al nombre 0, i aix�. 
//        t.setColorlletres(colorlletres);  //passar la llista feta al taulell
//        String[] etiquetes2={"Mines: " + mines};    //crear etiquetes per al taulell. Les etiquetes son text que sortir� a la dreta del taulell
//        f.setEtiquetes(etiquetes2);  //actualitzar les etiquetes
		f.setActetiquetes(false); // activar l�existencia d�etiquetes
		f.setTitle("Cercamines"); // t�tol del programa.
	}

	static int[][] crearMatriuPerLaGUI(String[][] tauler) {
		int[][] matriuGUI = new int[tauler.length][tauler[0].length];
		String[] caracters = {"[1]", "[2]", "[3]", "[4]", "[5]", "[6]", "[7]", "[8]", "[ ]", "[*]"};
		
		for (int i = 0; i < matriuGUI.length; i++) {	
			for (int j = 0; j < matriuGUI[0].length; j++) {
				for (int posicio = 0; posicio < caracters.length; posicio++) {
					if (tauler[i][j].equals(caracters[posicio])) {
						matriuGUI[i][j] = posicio + 1;
						
					}
				}
			}
		}

		return matriuGUI;
	}

}